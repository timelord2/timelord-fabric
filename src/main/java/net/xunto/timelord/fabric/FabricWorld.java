package net.xunto.timelord.fabric;

import static net.xunto.timelord.fabric.TimeLordFabric.DAYLIGHT_SYNC_OFFSET;
import static net.xunto.timelord.fabric.TimeLordFabric.DO_DAYLIGHT_SYNC;

import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.GameRules;
import net.minecraft.world.level.GameRules.BooleanValue;
import net.xunto.timelord.core.IWorld;

public class FabricWorld implements IWorld
{

  private final ServerLevel level;

  public FabricWorld(ServerLevel level) {
    this.level = level;
  }

  @Override
  public void setTime(long time) {
    this.level.setDayTime(time);
  }

  @Override
  public boolean shouldSync() {
    return this.level.getGameRules().getBoolean(DO_DAYLIGHT_SYNC);
  }

  @Override
  public int getSyncOffset() {
    return this.level.getGameRules().getInt(DAYLIGHT_SYNC_OFFSET);
  }

  @Override
  public void disableDefaultTimeCycle() {
    BooleanValue rule = this.level.getGameRules().getRule(GameRules.RULE_DAYLIGHT);
    rule.set(false, this.level.getServer());
  }
}
