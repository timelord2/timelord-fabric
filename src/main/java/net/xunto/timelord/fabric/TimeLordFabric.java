package net.xunto.timelord.fabric;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerTickEvents;
import net.fabricmc.fabric.api.gamerule.v1.GameRuleFactory;
import net.fabricmc.fabric.api.gamerule.v1.GameRuleRegistry;
import net.minecraft.world.level.GameRules;
import net.minecraft.world.level.GameRules.Category;
import net.xunto.timelord.core.IWorld;
import net.xunto.timelord.core.TimeLord;

public class TimeLordFabric implements ModInitializer {

  public static final GameRules.Key<GameRules.BooleanValue> DO_DAYLIGHT_SYNC =
      GameRuleRegistry.register("doDaylightSync", Category.MISC,
          GameRuleFactory.createBooleanRule(true));

  public static final GameRules.Key<GameRules.IntegerValue> DAYLIGHT_SYNC_OFFSET =
      GameRuleRegistry.register("daylightSyncOffset", Category.MISC,
          GameRuleFactory.createIntRule(0));

  @Override
  public void onInitialize() {
    ServerTickEvents.END_WORLD_TICK.register((world) -> {
      IWorld iworld = new FabricWorld(world);
      TimeLord.instance.worldTick(iworld);
    });
  }
}
